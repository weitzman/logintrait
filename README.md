## Deprecated

This project is no longer needed if you are using DTT 2.4.3+.


## Introduction

An add-on for [Drupal Test Traits](https://gitlab.com/weitzman/drupal-test-traits). Provides logout via user reset URL instead of forms. Useful when TFA/SAML are enabled. This project used to also provide a drupalLogin() implementation but that is longer needed as we got it into Drupal core in 10.3+. See https://www.drupal.org/project/drupal/issues/3469309.


## Installation

- Install via Composer. `composer require weitzman/logintrait`.

## Usage

- Use this trait whenever your test wants to login. See [ExampleLoginTest](https://gitlab.com/weitzman/logintrait/-/blob/master/src/ExampleLoginTest.php).

## Colophon

- **Author**: Created by [Moshe Weitzman](http://weitzman.github.io).
- **License**: Licensed under the [MIT license][mit]

[mit]: ./LICENSE
